import numpy as np

class Data:
  def __init__(self,value,error=None,minv=None,maxv=None):
    self.setValue(value)
    self.setError(error)
    self.setMin(minv)
    self.setMax(maxv)
    self.checkDimension()
  
  def setValue(self,value):
    self.value=value
  
  def setError(self,error):
    self.error=error
  
  def setMin(self,minv):
    self.minv=minv if min is not None else 0.
  
  def setMax(self,maxv):
    self.maxv=maxv
  
  def checkDimension(self):
    if self.error is not None:
      if np.shape(self.value)!=np.shape(self.error):
        raise RuntimeError( "Experimental values must have the same dimension as Experimental errors" )

class ExperimentReplicateData:
  def __init__(self):
    self.data={}
  
  def addData(self,symbol,data):
    self.data[symbol]=data

class Experiment:
  def __init__(self,replicates=[]):
    self.replicates=[]
    for r in replicates:
      self.addReplicate(r)
  
  def addReplicate(self,replicate):
    self.replicates.append(replicate)

class DataFileDescriptor:
  def __init__(self,filename,sheet=1,cols=[],header=[],delimiter=';'):
    self.filename=filename
    self.sheet=sheet
    self.cols=cols
    self.header=header
    self.delimiter=delimiter

class ExperimentReplicateDataLoader:
  def __init__(self,descriptors):
      self.descriptors={descriptor.filename:descriptor for descriptor in descriptors}
      self.raw_data={}
      self.readFiles()
        
  def readFile(self,descriptor):
    # return raw_data[filename]=np.array
    pass
    
  def readFiles(self):
    for filename,descr in self.descriptors.items():
      self.readFile(descr)
    
  def addDescriptor(self,descr):
    self.descriptors[descr.filename]=descr
    
  def updateHeader(self,filename,header):
    self.descriptors[filename].header=header
    
  def getReplicates(self):
    replicates=[]
    for filename,descr in self.descriptors.items():
      replicates.append(ExperimentReplicateData())
      pairs=[]
      for idx1,s in enumerate(descr.header):
        if s[-3:] not in ["err","min","max"]:
          try:
            idxerr=descr.header.index("{}_err".format(s))
          except:
            idxerr=None
          try:
            idxmin=descr.header.index("{}_min".format(s))
          except:
            idxmin=None
          try:
            idxmax=descr.header.index("{}_max".format(s))
          except:
            idxmax=None
          pairs.append((idx1,idxerr,idxmin,idxmax))
      for p in pairs:
        try:
          d=Data(self.raw_data[filename][:,p[0]].astype(np.float32))
        except:
          d=Data(self.raw_data[filename][:,p[0]])
        if p[1] is not None:
          d.setError(self.raw_data[filename][:,p[1]].astype(np.float32))
        if p[2] is not None:
          d.setMin(self.raw_data[filename][:,p[2]].astype(np.float32))
        if p[3] is not None:
          d.setMin(self.raw_data[filename][:,p[3]].astype(np.float32))
        replicates[-1].addData(descr.header[p[0]],d)
    return replicates

class ODSReplicateLoader(ExperimentReplicateDataLoader):
  def readFile(self,descriptor):
    from pyexcel_ods import get_data
    self.raw_data[descriptor.filename]=np.array(get_data(descriptor.filename,delimiter=descriptor.delimiter)[descriptor.sheet])[:,descriptor.cols]

if __name__=="__main__":
  loader_rep1=ODSReplicateLoader( [DataFileDescriptor(filename="../n1_rep1.ods",sheet="Sheet1",cols=[0,1],header=['t','pH'],delimiter=';') ] )
  loader_rep2=ODSReplicateLoader( [DataFileDescriptor(filename="../n1_rep2.ods",sheet="Sheet1",cols=[0,1],header=['t','pH'],delimiter=';') ] )
  rep1=loader_rep1.getReplicates()
  rep2=loader_rep2.getReplicates()
  
  print(rep2.data['t'].value)
  print(rep1.data['pH'].value)

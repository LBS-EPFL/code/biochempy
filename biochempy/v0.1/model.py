from biochempy.data import *
import numpy as np

# Model one experiment
class ModelReplicate:
  def __init__(self,replicate):
    self.replicate=replicate
    self.phase_descriptor={}
    self.dependencies=[]
  
  def addPhaseDescriptor(self,f_graph,phase='default'):
    self.phase_descriptor[phase]=f_graph
    self.dependencies.extend(f_graph.input_data)
    self.dependencies=list(set(self.dependencies))     

  def predict(self,initdata):
    init_data=dict(initdata)
    available_data=init_data.keys()
    exp_data=self.replicate.data.keys()
    for dep in self.dependencies:
      if dep not in available_data:
        if dep in exp_data:
          init_data[dep]=self.replicate.data[dep]
        else:
          raise RuntimeError("You need to provide a value for {}".format(dep))
    if 'protocol' in exp_data:
      init_data['protocol']=self.replicate.data['protocol']

    available_data=init_data.keys()
    if 't' in available_data:
      time=init_data['t'].value
      if 'protocol' in available_data:
        protocol=init_data['protocol'].value
        split_points=[]
        dynamics=[]
        for p in range(1,len(protocol)):
          if protocol[p]!=protocol[p-1]:
            split_points.append(p)
            dynamics.append(protocol[p-1])
        dynamics.append(protocol[-1])
        time=np.split(time,split_points)
      else:
        if len(self.phase_descriptor.keys())>1:
          raise RuntimeError("Multiple phases found but no protocol is defined")
        time=np.split(time,[])
        dynamics=[self.phase_descriptor.keys()[0]]
      
      # predict first phase
#      data=dict(init_data)
      init_data['t']=Data(time[0]-time[0][0])
      res=self.phase_descriptor[dynamics[0]].compute(init_data)
      for key,val in res.items():
        if key in init_data.keys():
          init_data[key]=Data(np.hstack( [init_data[key].value,val.value]) )
        else:
          init_data[key]=val
        if "{}_0".format(key) in init_data.keys():
          init_data["{}_0".format(key)]=Data(res[key].value[-1])
      # predict next phases after updating time and initial conditions
      for idx in range(1,len(dynamics)):
        init_data['t']=Data(time[idx]-time[idx-1][-1])
        res=self.phase_descriptor[dynamics[idx]].compute(init_data)
        for key,val in res.items():
          if key in init_data.keys():
            init_data[key]=Data(np.hstack( [init_data[key].value,val.value]) )
          else:
            init_data[key]=val
          if "{}_0".format(key) in init_data.keys():
            init_data["{}_0".format(key)]=Data(res[key].value[-1])
      init_data['t']=self.replicate.data['t']
      unwanted = set(init_data.keys()) - set(self.phase_descriptor[dynamics[-1]].observables+['t'])
      for unwanted_key in unwanted: 
        del init_data[unwanted_key]
      return init_data
    else:
      if len(self.phase_descriptor.keys())>1:
        raise RuntimeError("Multiple phases found but no protocol nor time is defined")
      res=self.phase_descriptor[list(self.phase_descriptor.keys())[0]].compute(init_data)
      unwanted = set(res.keys()) - set(self.phase_descriptor[list(self.phase_descriptor.keys())[0]].observables)
      for unwanted_key in unwanted: 
        del res[unwanted_key]
      return res
      
  def loss(self,init_data):
    res=self.predict(init_data)
    l=0.
    for key in res.keys():
      if self.replicate.data[key].minv is not None and self.replicate.data[key].maxv is not None:
        # data must be in specified range
        if np.any( np.less(res[key].value,self.replicate.data[key].minv) ) or np.any( np.greater(res[key].value,self.replicate.data[key].maxv) ):
          return np.inf
      elif self.replicate.data[key].minv is not None:
        # data must be bigger than threshold
        if np.any( np.less(res[key].value,self.replicate.data[key].minv) ):
          return np.inf
      elif self.replicate.data[key].maxv is not None:
        # data must be smaller than threshold
        if np.any( np.greater(res[key].value,self.replicate.data[key].maxv) ):
          return np.inf
      if self.replicate.data[key].error is not None:
        ds=((res[key].value - self.replicate.data[key].value)/self.replicate.data[key].error)
        l+= ds.dot(ds)/ds.size
    if not np.isfinite(l):
      return np.inf
    return 0.5*l
  
  def log_prob(self,init_data):
    return -self.loss(init_data)

class ModelExperiment:
  def __init__(self,experiment):
    self.experiment=experiment
    self.replicate_models=[ ModelReplicate(exp) for exp in self.experiment.replicates ]
  
  def addPhaseDescriptor(self,f_graph,phase='default'):
    for idx in range(len(self.replicate_models)):
      self.replicate_models[idx].addPhaseDescriptor(f_graph,phase)
  
  def correctInitData(self,init_data,idx):
    data=dict(init_data)
    suffix="_rep{}".format(idx+1)
    nsuffix=len(suffix)
    for d in init_data.keys():
      if d[-nsuffix:]==suffix:
        data[d[:-nsuffix]]=init_data[d]
    return data
  
  def predict(self,init_data):
    predictions=[]
    for idx in range(len(self.replicate_models)):
      data=self.correctInitData(init_data,idx)
      predictions.append( self.replicate_models[idx].predict(data) )
    return predictions
  
  def loss(self,init_data):
    l=[]
    for idx,m in enumerate(self.replicate_models):
      l.append(m.loss(self.correctInitData(init_data,idx)))
    return np.mean(l)

  def log_prob(self,init_data):
    l=[]
    for idx,m in enumerate(self.replicate_models):
      l.append(m.log_prob(self.correctInitData(init_data,idx)))
    return np.log(np.mean(np.exp(l)))
    
class Model:
  def __init__(self):
    self.experiments=[]
    
  def addExperiment(self,experiment):
    self.experiments.append(experiment)
  
  def predict(self,init_data):
    predictions=[]
    for idx in range(len(self.experiments)):
      predictions.append( self.experiments[idx].predict(init_data) )
    return predictions
  
  def loss(self,init_data):
    l=[]
    for idx,m in enumerate(self.experiments):
      l.append(m.loss(init_data))
    return np.sum(l)

  def log_prob(self,init_data):
    l=[]
    for idx,m in enumerate(self.experiments):
      l.append(m.log_prob(init_data))
    return np.sum(l)
    
if __name__=="__main__":
  from equations import *
  from functions import *
  import matplotlib.pyplot as plt
  
  # Data
  loader_rep1=ODSReplicateLoader( [DataFileDescriptor(filename="../n1_rep1.ods",sheet="Sheet1",cols=[0,1,2,3],header=['t','pH','pH_err','protocol'],delimiter=';') ] )
  loader_rep2=ODSReplicateLoader( [DataFileDescriptor(filename="../n1_rep1.ods",sheet="Sheet1",cols=[0,1,2,3],header=['t','pH','pH_err','protocol'],delimiter=';') ] )
  experiment=loader_rep1.getReplicate()
  
  init_data={'Ctot':Data(0.003,0.05), 'SP_0':Data(0.0003,0.05), 'MC_0':Data(0.001,0.05),
             'Ctot_rep1':Data(0.03,0.05), 'SP_0_rep1':Data(0.0003,0.05), 'MC_0_rep1':Data(0.001,0.05),
             'Ctot_rep2':Data(0.003,0.05), 'SP_0_rep2':Data(0.0003,0.05), 'MC_0_rep2':Data(0.001,0.05), 
             'km2':Data(0.1,None), 'k1':Data(10,None), 'k2':Data(0.1,None), 
             'k3':Data(0.1,None), 'km1':Data(1e8,None),
             'km3':Data(0.00001,None), 'knu':Data(10.,None)}
  
  # Function Graphs;
  constraints=["H=SP+MC+1e-7",
               "MCH=Ctot-SP-MC",
               "pH=-log10(H)",
              ]
  constraints=[Equation(s) for s in constraints]
  observables=['pH']
  ## Phase 'dark'
  equations_dark  =["MCH'=km1*MC*H+k3*SP*H-k1*MCH-km3*MCH",
                    "MC'=k1*MCH+km2*SP-km1*MC*H-k2*MC",
                    "SP'=k2*MC+km3*MCH-k3*SP*H-km2*SP",
                   ]
  equations_dark  =[Equation(s) for s in equations_dark]
  equations_dark  =applyConstraints(equations_dark,constraints)
  equation_dark_graph=EquationGraph(equations_dark)
  function_dark_graph=FunctionGraph(equation_dark_graph,observables)
  print("Final equations in 'dark' phase:")
  for e in equations_dark:
    print("\t{} = {}".format(e.lhs,e.rhs))
  print("Order in which variables are computed: ", [[o  for f in func for o in f.outvals ] for func in function_dark_graph.order ])
  print()
  ## Phase 'light'
  equations_light =["MCH'=km1*MC*H+k3*SP*H-k1*MCH-knu*MCH",
                    "MC'=k1*MCH+km2*SP-km1*MC*H-k2*MC",
                    "SP'=k2*MC+knu*MCH-k3*SP*H-km2*SP",
                   ]
  equations_light =[Equation(s) for s in equations_light]
  equations_light =applyConstraints(equations_light,constraints)
  equation_light_graph=EquationGraph(equations_light)
  function_light_graph=FunctionGraph(equation_light_graph,observables)
  print("Final equations in 'light' phase:")
  for e in equations_light:
    print("\t{} = {}".format(e.lhs,e.rhs))
  print("Order in which variables are computed: ", [[o  for f in func for o in f.outvals ] for func in function_light_graph.order ])
  
  #Model
  model=ModelReplicate(experiment)
  model.addPhaseDescriptor(function_dark_graph,'dark')
  model.addPhaseDescriptor(function_light_graph,'light')
  res=model.predict(init_data)
  for obs in observables:
    plt.plot(res['t'].value,res[obs].value,label=obs)
  plt.plot(experiment.data['t'].value,experiment.data['pH'].value,label="Exp")
  plt.xlabel("time (s)")
  plt.ylabel("concentration (mol)")
  plt.legend()
  plt.show()
  
  print("Loss: ", model.loss(init_data))
  
  print()
  print("Now with multiple replicates")
  print()
  
  replicate1=loader_rep1.getReplicate()  
  replicate2=loader_rep2.getReplicate()
  
  experiment=Experiment()
  experiment.addReplicate(replicate1)
  experiment.addReplicate(replicate2)
  
  model=ModelExperiment(experiment)
  model.addPhaseDescriptor(function_dark_graph,'dark')
  model.addPhaseDescriptor(function_light_graph,'light')
  
  res_multi=model.predict(init_data)
  for idx,res in enumerate(res_multi):
    for obs in observables:
      plt.plot(res['t'].value,res[obs].value,label=obs)
    plt.plot(experiment.replicates[idx].data['t'].value,experiment.replicates[idx].data['pH'].value,label="Exp")
    plt.xlabel("time (s)")
    plt.ylabel("concentration (mol)")
    plt.legend()
    plt.show()
  
  print("Loss: ", model.loss(init_data))
  
  print()
  print("Computing average time per prediction...")
  import time
  start_time = time.time()
  for i in range(50):
    model.predict(init_data)
  print("\t{} seconds per prediction".format( (time.time() - start_time)/50.))

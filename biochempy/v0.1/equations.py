import networkx as nx
import sympy
from sympy.parsing.sympy_parser import parse_expr

class Equation:
  def __init__(self,string):
    l,r=string.split('=')
    l=l.strip()

    self.isOde=(l[-1]=="'")
    self.lhs=l[:-1] if self.isOde else l
    self.rhs=parse_expr(r)
    self.dependencies=[]

  def computeDependencies(self):
    self.dependencies=[str(s) for s in self.rhs.atoms(sympy.Symbol)]
    if self.isOde:
      if  't' not in self.dependencies:
        self.dependencies.append('t')
      initval='{}_0'.format(self.lhs)
      if initval not in self.dependencies:
        self.dependencies.append(initval)
  
  def subs(self,expressions):
    for cstr in expressions:
      self.rhs=self.rhs.subs(sympy.Symbol(cstr.lhs),cstr.rhs)
    self.computeDependencies()

class EquationGraph:
  def __init__(self,equations):
    self.equations={eq.lhs:eq for eq in equations}
    self.dependencies=self._computeDependencies(equations)
    self.graph=self._buildGraph()
    self.order=self._computeOrder()
    
  def _computeDependencies(self,equations):
    for idx,eq in enumerate(equations):
      equations[idx].computeDependencies()
    dep={eq.lhs: set(eq.dependencies) for eq in equations}
    ode=set([eq.lhs for eq in equations if eq.isOde ])
    for eq in equations:
      for d in dep[eq.lhs]:
        if d in ode:
          dep[eq.lhs]=list(dep[eq.lhs])
          dep[eq.lhs].extend(ode)
          dep[eq.lhs]=list(dep[eq.lhs])
          break
     
    dep_new={}
    for k,v in dep.items():
      if k in ode:
        odedep=["{}_0".format(vv) for vv in v if vv in ode]
        deps=[vv for vv in v if vv not in ode]
        deps.extend(odedep)
        dep_new[k]=set(deps)
      else:
        dep_new[k]=v
    return dep_new
  
  def _buildGraph(self):
    G = nx.DiGraph()
    roots = set()
    for k,v in self.dependencies.items():
      if len(v)==0:
        roots.add(k)
      else:
        for vv in v:
          G.add_edge(k, vv)
    return G

  def _computeOrder(self):
    d=dict((k, set(self.dependencies[k])) for k in self.dependencies)
    r=[]
    while d:
      t=set(i for v in d.values() for i in v)-set(d.keys())
      t.update(k for k, v in d.items() if not v)
      r.append(t)
      d=dict(((k, v-t) for k, v in d.items() if len(v)))
    return r

  def getDependencyList(self,sources):
    deps={}
    for source in sources:
      deps[source]=[]
      for prereq, target in nx.dfs_edges(self.graph, source):
        deps[source].append(target)
    return deps
  
  def getFunctionOrder(self,sources,deps):
    d=[]
    for i,dep in deps.items():
      d.extend(dep)
    d=set(d)
    k= [set([v for v in l if v in sources or v in d]) for l in self.order]
    return [kk for kk in k if len(kk)]

def applyConstraints(equations,constraints):
  cstr_lhs=[]
  for cstr in constraints:
    cstr_lhs.append(cstr.lhs)
  cstr_lhs=frozenset(cstr_lhs)
  equations=[eq for eq in equations if eq.lhs not in cstr_lhs]
  for idx,eq in enumerate(equations):
    equations[idx].subs(constraints)
  equations.extend(constraints)
  return equations

if __name__=="__main__":
  print("This module is tested in functions.py")

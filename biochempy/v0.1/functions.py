from biochempy.data import *
from functools import partial
from scipy.integrate import odeint
import sympy

class Function:
  def __init__(self,outvals,f,signature,isOde=False):
    self.outvals=outvals
    self.f=f
    self.signature=signature
    self.isOde=isOde
  
  def compute(self,data,out_queue=None):
    if self.isOde:
      args={}
      args['y0']   = [data[ss].value for ss in self.signature[0]]
      args['t']    = data['t'].value
      args['args'] = tuple([ data[ss].value for ss in self.signature[2:]])
    else:
      args={ss:data[ss].value for ss in self.signature}
    d=self.f(**args)
    if out_queue is not None:
      out_queue.put( (self.outvals,d) )
    else:
      return (self.outvals,d)
  
class FunctionFactory:
  def __init__(self):
    pass
  
  def buildSympyGenericFunction(self,equation,dependencies):
    f=sympy.lambdify([sympy.Symbol(d) for d in dependencies],equation.rhs,'numpy')
    return Function([equation.lhs],f,dependencies[:])
  
  def buildODEFunction(self,equations,dependencies):
    def odesolver(ode,y0,t,args):
      return odeint(ode,y0,t,args=tuple(args))
    vals=[eq.lhs for eq in equations ]
    init=["{}_0".format(eq.lhs) for eq in equations ]
    deps=[[sympy.Symbol(d) for d in vals]]
    deps.append(sympy.Symbol('t'))
    deps.extend([sympy.Symbol(d) for d in dependencies if (d not in vals and d not in init and d!='t')])
    ode=sympy.lambdify(deps,[eq.rhs for eq in equations],'numpy')
    f=partial( odesolver, ode )
    deps=[[d for d in init]]
    deps.append('t')
    deps.extend([d for d in dependencies if (d not in vals and d not in init and d!='t')])
    
    return Function([eq.lhs for eq in equations],f,deps,isOde=True)
    
  def buildPythonGenericFunction(self,outvals,f,signature,isOde):
    return Function(outvals,f,signature[:],isOde=isOde)

class FunctionGraph:
  def __init__(self,equation_graph,observables):
    deplist=equation_graph.getDependencyList(observables)
    funcorder=equation_graph.getFunctionOrder(observables,deplist)
    factory=FunctionFactory()
    
    self.observables=observables
    self.input_data=funcorder[0]
    self.order=[]
    
    tmpdeps=[]
    for step in range(1,len(funcorder)):
      tmpdeps.extend(funcorder[step-1])
      ode=[]
      eqs=[]
      for lhs in funcorder[step]:
        equation=equation_graph.equations[lhs]
        if equation.isOde:
          ode.append(equation)
        else:
          eqs.append(equation)
      if len(ode):
        self.order.append( [factory.buildSympyGenericFunction(eq,tmpdeps) for eq in eqs]+[factory.buildODEFunction(ode,tmpdeps)] )
      elif len(eqs):
        self.order.append( [factory.buildSympyGenericFunction(eq,tmpdeps) for eq in eqs] )

  def compute(self,init_data):
    for dep in self.input_data:
      if dep not in init_data.keys():
        raise RuntimeError("Required data {} not provided".format(dep))
    
    outdata=dict(init_data)
    computed=[]
    for func in self.order:
      processes=[]
#      for f in func:
#        for s in f.signature:
#          if "{}_0".format(s) in init_data.keys():
#            print(s,outdata["{}_0".format(s)].value,init_data[s].value[-1])
#            outdata["{}_0".format(s)]=Data(np.copy(init_data[s].value[-1]))
        
#        processes.append(f.compute(outdata,None))
      processes=[f.compute(outdata,None) for f in func]
      for f in range(len(processes)):
        outvals,d=processes[f]
        computed.extend(outvals)
        if len(outvals)==1:
          outdata[outvals[0]]=Data(d)
        else:
          for idx,outval in enumerate(outvals):
            outdata[outval]=Data(d[:,idx])
    return { observable:outdata[observable] for observable in computed }

if __name__=="__main__":
  from equations import *
  import matplotlib.pyplot as plt
  
  equations  =["MCH'=km1*MC*H+k3*SP*H-k1*MCH-km3*MCH",
               "MC'=k1*MCH+km2*SP-km1*MC*H-k2*MC",
               "SP'=k2*MC+km3*MCH-k3*SP*H-km2*SP",
              ]

  constraints=["H=SP+MC+1e-7",
               "MCH=Ctot-SP-MC",
               "pH=-log10(H)",
              ]

  observables=['pH']
  
  equations  =[Equation(s) for s in equations]
  constraints=[Equation(s) for s in constraints]
  equations  =applyConstraints(equations,constraints)
  print("Final equations:")
  for e in equations:
    print("\t{} = {}".format(e.lhs,e.rhs))
  
  equation_graph=EquationGraph(equations)
  function_graph=FunctionGraph(equation_graph,observables)
  print("Order in which variables are computed: ", [[o  for f in func for o in f.outvals ] for func in function_graph.order ])
  init_data={'Ctot':Data(0.03,0.05), 'SP_0':Data(0.0003,0.05), 'MC_0':Data(0.01,0.05), 
             'km2':Data(0.01,None), 'k1':Data(0.01,None), 'k2':Data(0.01,None), 
             'k3':Data(0.01,None), 'km1':Data(0.01,None), 't':Data(np.linspace(0,100,1000),None), 
             'km3':Data(0.01,None)}
  
  
  res=function_graph.compute(init_data)
  for obs in observables:
    plt.plot(init_data['t'].value,res[obs].value,label=obs)
#  plt.plot(init_data['t'].value,res["MCH"].value+res["MC"].value+res["SP"].value,label="total")
  plt.xlabel("time (s)")
  plt.ylabel("concentration (mol)")
  plt.legend()
  plt.show()

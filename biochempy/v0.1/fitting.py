from biochempy.data import *
from lmfit import minimize, Parameters, conf_interval, Minimizer
import numpy as np
#import dill
#import pathos

class Optimizer:
  def __init__(self,model,init_data,variables):
    self.initial_params=dict(init_data)
    self.constants=dict(init_data)
    self.params=Parameters()
    for variable in init_data:
      if variable in variables:
        self.params.add(variable,value=init_data[variable].value,min=init_data[variable].minv if init_data[variable].minv is not None else 0.,max=init_data[variable].maxv,vary=True)
        del self.constants[variable]
      else:
        self.params.add(variable,value=init_data[variable].value,min=init_data[variable].minv if init_data[variable].minv is not None else 0.,max=init_data[variable].maxv,vary=False)
    self.variables=[var for var in variables if var not in self.constants.keys()]
    self.model=model
  
  def loss(self,params,infinity=9999.):
    const=dict(self.constants)
    for p in self.variables:
      if self.initial_params[p].minv is not None and self.initial_params[p].maxv is not None:
        # data must be in specified range
        if np.any( np.less(params[p].value,self.initial_params[p].minv) ) or np.any( np.greater(params[p].value,self.initial_params[p].maxv) ):
          return infinity
      elif self.initial_params[p].minv is not None:
        # data must be bigger than threshold
        if np.any( np.less(params[p].value,self.initial_params[p].minv) ):
          return infinity
      elif self.initial_params[p].maxv is not None:
        # data must be smaller than threshold
        if np.any( np.greater(params[p].value,self.initial_params[p].maxv) ):
          return infinity
      const[p]=Data(params[p].value,self.initial_params[p].error)      
      
    l=self.model.loss(const)
    for v in self.variables:
      if self.initial_params[v].error is not None:
        l+=(const[v].value-self.initial_params[v].value)**2/self.initial_params[v].error**2
    if not np.isfinite(l):
      return infinity
    return l
  
  def listToParams(self,values):
    const=dict(self.constants)
    for idx,p in enumerate(self.variables):
      const[p]=Data(values[idx],self.initial_params[p].error,self.initial_params[p].minv,self.initial_params[p].maxv)
    return const
    
  def log_prob(self,params):
    const=self.listToParams(params)
    return -self.loss(const,infinity=np.inf)
    
  def minimize(self,method='nelder',maxiter=200,params=None):
    if method=="nelder":
      fit_opt={'maxiter':maxiter,'adaptive':True}
    elif method=="lbfgsb":
      fit_opt={'maxiter':maxiter,'ftol': 1e-10, 'gtol': 1e-08, 'eps': 1e-10}
    else:
      fit_opt={}
    if params is None:
      return minimize(self.loss,self.params,method=method,options=fit_opt)
    else:
      return minimize(self.loss,params,method=method,options=fit_opt)
    
  def sampleMCMC(self,result,workers=1,burn=0,steps=200,thin=1,nwalkers=50,progress=True):
    import emcee
    if workers>1:
      import os
      os.environ["OMP_NUM_THREADS"] = "1"
      from pathos.multiprocessing import ProcessingPool as Pool
      pool = Pool(workers)
    else:
      pool=None
    
    pos=np.zeros((nwalkers,result.nvarys))
    
    params=result.params
    for idx,v in enumerate(self.variables):
      if self.initial_params[v].minv is not None and self.initial_params[v].maxv is not None:
        rnd=np.random.uniform(size=nwalkers,low=self.initial_params[v].minv-params[v].value,high=self.initial_params[v].maxv-params[v].value)
        pos[:,idx] = (params[v].value + rnd)
      elif self.initial_params[v].minv is not None and self.initial_params[v].maxv is None:
        maxx=self.initial_params[v].error if self.initial_params[v].error is not None else params[v].value*0.05
        rnd=np.random.uniform(size=nwalkers,low=self.initial_params[v].minv-params[v].value,high=maxx)
        pos[:,idx] = (params[v].value + rnd)
      elif self.initial_params[v].minv is None and self.initial_params[v].maxv is not None:
        minx=-self.initial_params[v].error if self.initial_params[v].error is not None else -params[v].value*0.05
        rnd=np.random.uniform(size=nwalkers,high=self.initial_params[v].maxv-params[v].value,low=minx)
        pos[:,idx] = (params[v].value + rnd)
      elif self.initial_params[v].error is not None:
        rnd=np.random.random((nwalkers))
        pos[:,idx] = (params[v].value + rnd*self.initial_params[v].error)
      else:
        rnd=np.random.random((nwalkers))
        pos[:,idx] = (params[v].value + rnd*params[v].value*0.05)
      
      
    
    sampler = emcee.EnsembleSampler(nwalkers, result.nvarys,self.log_prob,pool=pool)
    sampler.run_mcmc(pos, steps*thin+burn, progress=progress,skip_initial_state_check=True)
    flat_samples = sampler.get_chain(discard=burn, thin=thin, flat=True)
    
    try:
      tau=sampler.get_autocorr_time(c=1)
    except:
      tau=None
    return flat_samples,result.var_names,[params[v].value for v in result.var_names],tau
 
if __name__=="__main__":
  from model import *
  from functions import *
  from equations import *
  from data import *
  import matplotlib.pyplot as plt
  import corner
  from lmfit import printfuncs,fit_report
  # Data
  loader_rep1=ODSReplicateLoader( [DataFileDescriptor(filename="../n1_rep1.ods",sheet="Sheet1",cols=[0,1,2,3],header=['t','pH','pH_err','protocol'],delimiter=';') ] )
  
  init_data={#'Ctot':Data(0.003,0.05), 'SP_0':Data(0.0003,0.05), 'MC_0':Data(0.001,0.05),
             'Ctot_rep1':Data(0.03,0.0001,maxv=0.0300000001), 'SP_0_rep1':Data(0.0003,0.00003), 'MC_0_rep1':Data(0.001,0.0001),
             'Ctot_rep2':Data(0.003,0.003), 'SP_0_rep2':Data(0.0003,0.0003), 'MC_0_rep2':Data(0.001,0.001), 
             'km2':Data(0.1,0.01), 'k1':Data(10.,1.), 'k2':Data(0.1,0.01), 
             'k3':Data(0.1,0.01), 'km1':Data(1.e8,1.e7),
             'km3':Data(0.00001,0.000001), 'knu':Data(.01,.001)}
  
  # Function Graphs;
  constraints=["H=SP+MC+1e-7",
               "MCH=Ctot-SP-MC",
               "pH=-log10(H)",
              ]
  constraints=[Equation(s) for s in constraints]
  observables=['pH']
  ## Phase 'dark'
  equations_dark  =["MCH'=km1*MC*H+k3*SP*H-k1*MCH-km3*MCH",
                    "MC'=k1*MCH+km2*SP-km1*MC*H-k2*MC",
                    "SP'=k2*MC+km3*MCH-k3*SP*H-km2*SP",
                   ]
  equations_dark  =[Equation(s) for s in equations_dark]
  equations_dark  =applyConstraints(equations_dark,constraints)
  equation_dark_graph=EquationGraph(equations_dark)
  function_dark_graph=FunctionGraph(equation_dark_graph,observables)
  print("Final equations in 'dark' phase:")
  for e in equations_dark:
    print("\t{} = {}".format(e.lhs,e.rhs))
  print("Order in which variables are computed: ", [[o  for f in func for o in f.outvals ] for func in function_dark_graph.order ])
  print()
  ## Phase 'light'
  equations_light =["MCH'=km1*MC*H+k3*SP*H-k1*MCH-knu*MCH",
                    "MC'=k1*MCH+km2*SP-km1*MC*H-k2*MC",
                    "SP'=k2*MC+knu*MCH-k3*SP*H-km2*SP",
                   ]
  equations_light =[Equation(s) for s in equations_light]
  equations_light =applyConstraints(equations_light,constraints)
  equation_light_graph=EquationGraph(equations_light)
  function_light_graph=FunctionGraph(equation_light_graph,observables)
  print("Final equations in 'light' phase:")
  for e in equations_light:
    print("\t{} = {}".format(e.lhs,e.rhs))
  print("Order in which variables are computed: ", [[o  for f in func for o in f.outvals ] for func in function_light_graph.order ])
  
  variables=['Ctot_rep1','SP_0_rep1','MC_0_rep1','k1','k2','k3','km1','km2','km3','knu']
#  variables=['SP_0_rep1','knu']
  print("Variables to optimize:")
  print(variables)
  
  replicate1=loader_rep1.getReplicate()  
#  replicate2=loader_rep2.getReplicate()
  
  experiment=Experiment()
  experiment.addReplicate(replicate1)
#  experiment.addReplicate(replicate2)
  
  modelexp=ModelExperiment(experiment)
  modelexp.addPhaseDescriptor(function_dark_graph,'dark')
  modelexp.addPhaseDescriptor(function_light_graph,'light')
  
  model=Model()
  model.addExperiment(modelexp)

  opt=Optimizer(model,init_data,variables)  
  res=opt.minimize(maxiter=400)
  best_params=res.params
  
  print()
  print("Optimized parameters:")
  print(best_params.pretty_print())
  
  final_parameters=dict(opt.constants)
  final_parameters.update(dict(best_params))
  
  res=model.predict(final_parameters)[0][0]
  initial=model.predict(init_data)[0][0]
  for obs in observables:
    plt.plot(res['t'].value,initial[obs].value,label="{} initial".format(obs))
    plt.plot(res['t'].value,res[obs].value,label="{} final".format(obs))
  plt.plot(experiment.replicates[0].data['t'].value,experiment.replicates[0].data['pH'].value,label="Exp")
  plt.xlabel("time (s)")
  plt.ylabel("concentration (mol)")
  plt.legend()
  plt.show()
  
  print()
  print("Now refining...")
  print()
  
  opt=Optimizer(model,{k:Data(v.value,init_data[k].error,init_data[k].minv,init_data[k].maxv) for k,v in final_parameters.items()},variables)  
  res=opt.minimize(method='lbfgsb',maxiter=400)
  best_params=res.params
  
  print()
  print("Optimized parameters:")
  print(best_params.pretty_print())
  print("Report:")
  print(fit_report(res))
  
  final_parameters=dict(opt.constants)
  final_parameters.update(dict(best_params))
  
  res1=model.predict(final_parameters)[0][0]
  initial=model.predict(init_data)[0][0]
  for obs in observables:
    plt.plot(res1['t'].value,initial[obs].value,label="{} initial".format(obs))
    plt.plot(res1['t'].value,res1[obs].value,label="{} final".format(obs))
  plt.plot(experiment.replicates[0].data['t'].value,experiment.replicates[0].data['pH'].value,label="Exp")
  plt.xlabel("time (s)")
  plt.ylabel("concentration (mol)")
  plt.legend()
  plt.show()
  
  print()
  print("Computing errors using emcee...")
  print()
  
  res = opt.sampleMCMC(res)
  
  emcee_plot = corner.corner(res[0], labels=res[1],truths=res[2])
  plt.show()
  
  for i in range(len(res[1])):
    mcmc = np.percentile(res[0][:, i], [16, 50, 84])
    txt = "{}: {} [{} {}]".format(res[1][i],mcmc[1],mcmc[0],mcmc[2])
    print(txt)
  

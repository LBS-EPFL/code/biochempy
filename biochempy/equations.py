import sympy
from sympy.parsing.sympy_parser import parse_expr

class Equation:
  def __init__(self,string):
    l,r=string.split('=')
    l=l.strip()

    self.dout_dt=(l[-1]=="'")
    self.lhs=l[:-1] if self.dout_dt else l
    self.rhs=parse_expr(r)
    self.dependencies=[]
    self.computeDependencies()
    
  def computeDependencies(self):
    self.dependencies=[str(s) for s in self.rhs.atoms(sympy.Symbol)]
    if self.dout_dt:
      if  't' not in self.dependencies:
        self.dependencies.append('t')
      initval='{}_0'.format(self.lhs)
      if initval not in self.dependencies:
        self.dependencies.append(initval)
  
  def subs(self,expressions):
    for cstr in expressions:
      self.rhs=self.rhs.subs(sympy.Symbol(cstr.lhs),cstr.rhs)
    self.computeDependencies()

def applyConstraints(equations,constraints):
  cstr_lhs=[]
  for cstr in constraints:
    cstr_lhs.append(cstr.lhs)
  cstr_lhs=frozenset(cstr_lhs)
  equations=[eq for eq in equations if eq.lhs not in cstr_lhs]
  for idx,eq in enumerate(equations):
    equations[idx].subs(constraints)
  equations.extend(constraints)
  return equations

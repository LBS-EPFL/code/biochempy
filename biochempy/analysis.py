import numpy as np
from biochempy.data import *

def errorEstimationEmcee(flat_samples,variables):
  p16=[]
  p50=[]
  p84=[]
  for i,var in enumerate(variables):
    mcmc = np.percentile(flat_samples[:, i], [16, 50, 84])
    p16.append(mcmc[0])
    p50.append(mcmc[1])
    p84.append(mcmc[2])
  return variables,p16,p50,p84

def errorPropagationEmcee(opt,nsamples):
  inds = np.random.randint(len(opt.emcee_flat_samples), size=nsamples)
  params=[opt.listToParams(opt.emcee_flat_samples[i]) for i in inds]
  predictions_mcmc=[[{k:[] for k in repl.data.keys()} for repl in exp.replicates] for exp in opt.model.experiments]
  for param in params:
    p=opt.model.predict(param)
    for iexp,exp in enumerate(p):
      for irepl,repl in enumerate(exp):
        for key in repl.keys():
          try:
            predictions_mcmc[iexp][irepl][key].append(repl[key].value)
          except:
            pass

  predictions_mcmc    =[[{k:v for k,v in repl.items() if len(v)>0} for repl in exp] for exp in predictions_mcmc]
  predictions_mcmc_p5 =[[{k:v for k,v in repl.items() if len(v)>0} for repl in exp] for exp in predictions_mcmc]
  predictions_mcmc_p16=[[{k:v for k,v in repl.items() if len(v)>0} for repl in exp] for exp in predictions_mcmc]
  predictions_mcmc_p50=[[{k:v for k,v in repl.items() if len(v)>0} for repl in exp] for exp in predictions_mcmc]
  predictions_mcmc_p84=[[{k:v for k,v in repl.items() if len(v)>0} for repl in exp] for exp in predictions_mcmc]
  predictions_mcmc_p95=[[{k:v for k,v in repl.items() if len(v)>0} for repl in exp] for exp in predictions_mcmc]
  
  for iexp,exp in enumerate(predictions_mcmc):
    for irepl,repl in enumerate(exp):
      for key in repl.keys():
        if len(predictions_mcmc[iexp][irepl][key])==0:
          continue
        if key=='t':
          del predictions_mcmc_p5[iexp][irepl][key]
        else:
          qnt=np.quantile(np.stack(predictions_mcmc[iexp][irepl][key],axis=1),[0.05,0.16,0.50,0.84,0.95],axis=1)
          predictions_mcmc_p5[iexp][irepl][key]=Data(qnt[0])
          predictions_mcmc_p16[iexp][irepl][key]=Data(qnt[1])
          predictions_mcmc_p50[iexp][irepl][key]=Data(qnt[2])
          predictions_mcmc_p84[iexp][irepl][key]=Data(qnt[3])
          predictions_mcmc_p95[iexp][irepl][key]=Data(qnt[4])
  
  return predictions_mcmc_p5,predictions_mcmc_p16,predictions_mcmc_p50,predictions_mcmc_p84,predictions_mcmc_p95

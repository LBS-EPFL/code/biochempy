from biochempy.data import Data
from biochempy.function import ComputationGraph
import numpy as np

class Replicate:
  def __init__(self, data, computation_graphs_init, computation_graphs, observables): # functions is a dict {phase:list_of_functions}
    self.data=data
    self.computation_graphs_init=computation_graphs_init
    self.computation_graphs=computation_graphs
    self.observables=observables
    init_phase='default' if 'protocol' not in self.data.keys() else self.data['protocol'].value[0]
    
    self.expected_indata=[obs for phase in self.computation_graphs_init.keys() for obs in computation_graphs_init[phase].getFunctionOrder(observables)[0] if obs[-2:]!="_0" ]
    self.expected_indata=list(set(self.expected_indata))
    self.expected_indata.extend( [obs for obs in computation_graphs_init[init_phase].getFunctionOrder(observables)[0] if obs[-2:]=="_0" ] )
    self.expected_indata=list(set(self.expected_indata))
    self.likelihood=np.inf

  def predict(self,indata):
    auxdata=dict(indata) # copy data to prevent global changes
    
    # extend input data with data data from replicate
    data_k=self.data.keys()
    auxdata_k=auxdata.keys()
    for expected in self.expected_indata:
      if expected not in auxdata_k:
        if expected in data_k:
          auxdata[expected]=self.data[expected]
        else:
          raise RuntimeError("You need to provide a value for {}".format(expected))
    if 'protocol' in data_k and 'protocol' not in auxdata_k:
      auxdata['protocol']=self.data['protocol']
    
    available_data=auxdata.keys()
    # predict time-dependent quantities
    if 't' in available_data:
      time=auxdata['t'].value
      if 'protocol' in available_data:
        protocol=auxdata['protocol'].value
        split_points=[]
        phases=[]
        for p in range(1,len(protocol)):
          if protocol[p]!=protocol[p-1]:
            split_points.append(p)
            phases.append(protocol[p-1])
        phases.append(protocol[-1])
        time=np.split(time,split_points)
      else:
        if len(self.computation_graphs_init.keys())>1:
          raise RuntimeError("Multiple phases found but no protocol is defined")
        time=np.split(time,[])
        phases=[list(self.computation_graphs_init.keys())[0]]

      # predict first phase using computation_graphs_init
      auxdata['t']=Data(time[0]-time[0][0])
      res=self.computation_graphs_init[phases[0]].compute(self.observables,auxdata)
      for key,val in res.items():
        if np.ndim(val.value)>1:
          val.value=val.value.flatten()
        if key in auxdata.keys():
          auxdata[key]=Data(np.hstack( [auxdata[key].value,val.value]) )
        else:
          auxdata[key]=val
        if "{}_0".format(key) in auxdata_k:
          auxdata["{}_0".format(key)]=Data(res[key].value[-1])
      # predict all other phases using computation_graphs
      for idx in range(1,len(phases)):
        auxdata['t']=Data(time[idx]-time[idx-1][-1])
        res=self.computation_graphs[phases[idx]].compute(self.observables,auxdata)
        for key,val in res.items():
          if np.ndim(val.value)>1:
            val.value=val.value.flatten()
          if key in auxdata.keys():
            auxdata[key]=Data(np.hstack( [auxdata[key].value,val.value]) )
          else:
            auxdata[key]=val
          if "{}_0".format(key) in auxdata.keys():
            auxdata["{}_0".format(key)]=Data(res[key].value[-1])
      auxdata['t']=self.data['t']
      unwanted = set(auxdata.keys()) - set(self.observables+['t'])
      for unwanted_key in unwanted: 
        del auxdata[unwanted_key]
      return auxdata
    # predict other quantities
    else:
      if len(self.computation_graphs.keys())>1:
        raise RuntimeError("Multiple phases found but no protocol nor time is defined")
      phase=list(self.computation_graphs_init.keys())[0]
      res=self.computation_graphs_init[phase].compute(self.observables,auxdata)
      unwanted = unwanted = set(res.keys()) - set(self.observables)
      for unwanted_key in unwanted: 
        del res[unwanted_key]
      return res
  
  def loss(self,indata):
    res=self.predict(indata)
    l=0.
    for key in res.keys():
      if key not in self.data.keys() or key=='t':
        continue
      if self.data[key].minv is not None and self.data[key].maxv is not None:
        # data must be in specified range
        if np.any( np.less(res[key].value,self.data[key].minv) ) or np.any( np.greater(res[key].value,self.data[key].maxv) ):
          return np.inf
      elif self.data[key].minv is not None:
        # data must be bigger than threshold
        if np.any( np.less(res[key].value,self.data[key].minv) ):
          return np.inf
      elif self.data[key].maxv is not None:
        # data must be smaller than threshold
        if np.any( np.greater(res[key].value,self.data[key].maxv) ):
          return np.inf
      if self.data[key].error is not None:
        ds=((res[key].value - self.data[key].value)/self.data[key].error)
        l+= ds.dot(ds)/ds.size
    self.likelihood=0.5*l
    return self.likelihood

class Experiment:
  def __init__(self, functions, observables, name=''):
    self.functions=functions
    functions_no0={k:[ vv for vv in v if vv.outvals[0][-2:]!="_0" ] for k,v in functions.items()}
    self.computation_graphs_init={k:ComputationGraph(v) for k,v in functions.items() }
    self.computation_graphs={k:ComputationGraph(v) for k,v in functions_no0.items() }
    self.observables=observables
    self.replicates=[]
    self.name=name
  
  def setName(self,name):
    self.name=name
  
  def addReplicate(self,data):
    self.replicates.append( Replicate(data,self.computation_graphs_init,self.computation_graphs,self.observables) )
  
  def correctKeys(self,indata,idx):
    data=dict(indata)
    suffix="_rep{}".format(idx+1)
    nsuffix=len(suffix)
    for d in indata.keys():
      if d[-nsuffix:]==suffix:
        data[d[:-nsuffix]]=indata[d]
    return data
  
  def predict(self,indata):
    predictions=[]
    for idx in range(len(self.replicates)):
      predictions.append( self.replicates[idx].predict( self.correctKeys(indata,idx) ) )
    return predictions
  
  def loss(self,indata):
    l=0.
    n=0
    for idx,m in enumerate(self.replicates):
      l+=m.loss(self.correctKeys(indata,idx))
      n+=1
    return l/n

class Model:
  def __init__(self):
    self.experiments=[]
    
  def addExperiment(self,experiment):
    self.experiments.append(experiment)
    
  def predict(self,indata):
    predictions=[]
    for idx in range(len(self.experiments)):
      predictions.append( self.experiments[idx].predict(indata) )
    return predictions
  
  def loss(self,indata):
    l=0.
    for idx,m in enumerate(self.experiments):
      l+=m.loss(indata)
    return l

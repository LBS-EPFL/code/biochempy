from biochempy.data import Data
from functools import partial
import numpy as np
from scipy.integrate import odeint
import sympy

class Function:
  def __init__(self,outvals,invals,f,dout_dt=False):
    self.outvals=outvals
    self.invals=[i for i in invals if i not in self.outvals]
    self.f=f
    self.dout_dt=dout_dt

  def compute(self,data,out_queue=None):
    if self.dout_dt:
      args={}
      args['y0']   = [data[ss].value for ss in self.invals[0]]
      args['t']    = data['t'].value
      args['args'] = tuple([ data[ss].value for ss in self.invals[2:]])
    else:
      args={ss:data[ss].value for ss in self.invals}
    d=self.f(**args)
    if out_queue is not None:
      out_queue.put( (self.outvals,d) )
    else:
      return (self.outvals,d)

  def __str__(self):
    try:
      fname=self.f.__name__
    except:
      fname='f'
    return "{}:[{}]->[{}]".format( fname,",".join(map(str,self.invals)),",".join(self.outvals) )

class FunctionFactory:
  def __init__(self):
    pass
  
  def buildSympyGenericFunction(self,equation,dependencies):
    f=sympy.lambdify([sympy.Symbol(d) for d in dependencies],equation.rhs,'numpy')
    return Function([equation.lhs],dependencies[:],f)
  
  def buildODEFunction(self,equations,dependencies):
    def odesolver(ode,y0,t,args):
      if np.ndim(y0)==0:
        y0=np.array(y0,ndmin=1)
      if np.ndim(y0)>1:
        y0=np.array(y0,ndmin=1).flatten()
      
      return odeint(ode,y0,t,args=tuple(args))
    vals=[eq.lhs for eq in equations ]
    init=["{}_0".format(eq.lhs) for eq in equations ]
    deps=[[sympy.Symbol(d) for d in vals]]
    deps.append(sympy.Symbol('t'))
    deps.extend([sympy.Symbol(d) for d in dependencies if (d not in vals and d not in init and d!='t')])
    ode=sympy.lambdify(deps,[eq.rhs for eq in equations],'numpy')
    f=partial( odesolver, ode )
    deps=[[d for d in init]]
    deps.append('t')
    deps.extend([d for d in dependencies if (d not in vals and d not in init and d!='t')])
    
    return Function([eq.lhs for eq in equations],deps,f,dout_dt=True)
    
  def buildPythonGenericFunction(self,outvals,signature,f,dout_dt):
    return Function(outvals,signature[:],f,dout_dt=dout_dt)

class ComputationGraph:
  def __init__(self,functions,observables=[]):
    self.functions={idx:f for idx,f in enumerate(functions)}
    self.dependencies=self._computeDependencies()
    self.order=self._computeOrder()
    
    
  def _computeDependencies(self):
    fmap={}
    deps={}
    for fidx,f in self.functions.items():
      for o in f.outvals:
        fmap[o]=fidx
      deps[fidx]=[ val for val in f.invals if type(val)==str]
      if len(f.invals):
        if type(f.invals[0])==list:
          deps[fidx].extend(f.invals[0])
    for fidx,deplist in deps.items():
      deps[fidx]=list(set([fmap[dep] if dep in fmap.keys() else dep for dep in deplist]))
    return deps

  def _computeOrder(self):
    d=dict((k, set(self.dependencies[k])) for k in self.dependencies)
    r=[]
    while d:
      t=set(i for v in d.values() for i in v)-set(d.keys())
      t.update(k for k, v in d.items() if not v)
      r.append(t)
      d=dict(((k, v-t) for k, v in d.items() if len(v)))
    return r
  
  def getFunctionOrder(self,sources):
    d=[]
    for s in sources:
      for fidx,f in self.functions.items():
        if s in f.outvals:
          d.append(fidx)
    for n in range(1,2*len(self.order)):
      for dd in list(set(d)):
        if dd in self.functions.keys():
          d.append(dd)
          d.extend( list([i for i in self.functions[dd].invals if type(i)!=list]) )
          d.extend( list([ii  for i in self.functions[dd].invals for ii in i if type(i)==list]) )
        else:
          for fidx,f in self.functions.items():
            if dd in f.outvals:
              d.append(fidx)
    d=set(d)
    k= [set([v for v in l if v in sources or v in d ]) for l in self.order]
    k[0]=set([v for v in k[0] if type(v)==str ])
    return [kk for kk in k if len(kk)]    

  def compute(self,observables,indata):
    order=self.getFunctionOrder(observables)
    for var in order[0]:
      if var not in indata.keys():
        raise RuntimeError("You must provide a value for {}".format(var))
    data=dict(indata)
    computed=[]
    
    for func in order[1:]:
      processes=[]
      processes=[self.functions[f].compute(data,None) for f in func]
      for ridx,result in enumerate(processes):
        outvals,d=result
        computed.extend(outvals)
        if len(outvals)==1:
          data[outvals[0]]=Data(d)
        else:
          for idx,outval in enumerate(outvals):
            data[outval]=Data(d[:,idx])
    return { observable:data[observable] for observable in computed }

if __name__=="__main__":
  def obs():
    return 0
  a=Function(['a','b'],['c','t','s'],obs)
  b=Function(['c'],['d','t','r'],None)
  c=Function(['e','d'],['f','p'],None)
  
  print(a)
  d=ComputationGraph([a,b,c])
  print(d.dependencies)
  print(d.order)
  print(d.getFunctionOrder(['c']))

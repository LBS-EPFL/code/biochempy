from biochempy.analysis import *
from collections import OrderedDict
import os
from biochempy.model import *
import dill
    
def saveToOds( data, folder, name ): # data is {sheet:data,header}
  from pyexcel_io import save_data
  if name[-4:]!=".ods":
    name+=".ods"
  if not os.path.exists(folder):
    os.makedirs(folder)
  filename=os.path.join(folder,name)
  save_data(filename,data,library='pyexcel-ods')    

def saveToTxt( data, folder, name ):
  # use tabulate and save tabular files in folder, one for every sheet
  pass

def saveParameters( params, filename, fmt='dump' ):
  if fmt=='dump':
    with open(filename,'w') as outfile:
      params.dump(outfile)
  
  elif fmt=='txt':
    from tabulate import tabulate
    rows=[]
    for key in params.valuesdict():
      rows.append( [key,params[key].value] )
    with open(filename,'w') as outfile:
      outfile.write(tabulate(rows, headers=['Name', 'Value']))
  else:
    raise RuntimeError("Accepted values are 'dump' and 'txt'. Provided value: {}".format(fmt))
    
def loadParameters( filename ):
  from lmfit import Parameters
  params=Parameters()
  with open(filename) as infile:
    params.load(infile)
  return params

def predictionsToDict(predictions,model): # predictions is a list of tuples (pred,suffix)
  summary={}
  keys={}
  for iexp,exp in enumerate(model.experiments):
    expname=model.experiments[iexp].name
    nrows=0
    if expname not in summary.keys():
      keys[expname]=[]
      for irepl,repl in enumerate(exp.replicates):
        prefix="#repl{}_".format(irepl)
        for key in repl.data.keys():
          keys[expname].append(prefix+key+"_expval")
          nrows=max(nrows,np.size(repl.data[key].value))
      for ipred,pred in enumerate(predictions):
        for irepl,repl in enumerate(exp.replicates):
          prefix="#repl{}_".format(irepl)
          for key in repl.data.keys():
            if key!='t' and key!='protocol' and key in pred[0][iexp][irepl].keys():
              keys[expname].append(prefix+key+pred[1])
      summary[expname]=[ keys[expname] ]
      for i in range(nrows):
        row=[]
        for irepl,repl in enumerate(exp.replicates):
          prefix="#repl{}_".format(irepl)
          for key in repl.data.keys():
            try:
              if key=='protocol':
                row.append(repl.data[key].value[i])
              else:
                row.append(float(repl.data[key].value[i]))
            except:
              row.append('')
        for ipred,pred in enumerate(predictions):
          for irepl,repl in enumerate(exp.replicates):
            for key in repl.data.keys():
              if key!='t' and key!='protocol' and key in pred[0][iexp][irepl].keys():
                try:
                  row.append(float(pred[0][iexp][irepl][key].value[i]))
                except:
                  row.append('')
        summary[expname].append(row)
        
  return summary


def reportOptimizedModel( opt, nsamples=1000 ):
  data = OrderedDict()
  # report optimized params:
  pi=opt.initial_params.valuesdict()
  po=opt.optimal_params.valuesdict()
  rows=[]
  rows.append( ["#name","#init_val","#final_val","#variable"] )
  for k in pi.keys():
    rows.append( [k, float(pi[k]), float(po[k]), "True" if k in opt.variables else "False"] )  
  data.update( {"Fit_Report":rows} )
  # report fit predictions:
  pred=opt.model.predict(opt.optimal_params)
  data.update(predictionsToDict([(pred,"_fit")],opt.model))
  
  dataMC=None
  if opt.emcee_flat_samples is not None:
    dataMC = OrderedDict()
    # report emcee results:
    varname,p16,p50,p84 = errorEstimationEmcee(opt.emcee_flat_samples,opt.variables)
    rows=[]
    rows.append( ["#name","#16th perc.","#50th perc.","#84th perc."] )
    for k,var in enumerate(varname):
      rows.append( [var, float(p16[k]), float(p50[k]), float(p84[k])] )
    dataMC.update( {"MC_Report":rows} )
    
    # report emcee predictions:
    v5,v16,v50,v84,v95=errorPropagationEmcee(opt,nsamples=nsamples)
    dataMC.update( predictionsToDict( [(v5,"_perc5th"),(v16,"_perc16th"),(v50,"_perc50th"),(v84,"_perc84th"),(v95,"_perc95th")],opt.model ) )
  return data,dataMC

def dump(obj,filename):
  import dill
  with open(filename, "wb") as dill_file:
    dill.dump(obj, dill_file)

def load(filename):
  import dill
  with open(filename,'rb') as dill_file:
    return dill.load(dill_file)
